// Only include file once
#pragma once

#include "BearLibTerminal.h"

/**
 * Handle the events specific to application, for example
 * close the terminal or pause application.
 *
 * @param key: Key pressed.
 */
void HandleEvent(int key);
